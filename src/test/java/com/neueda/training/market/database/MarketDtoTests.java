package com.neueda.training.market.database;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Unit Tests for MarketDto.
 *
 * @author Neueda
 */
public class MarketDtoTests {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private static final String TEST_TICKER = "FTSE";

	@Test
	public void defaultConstructor_gettersAndSetters() throws Exception {
		MarketDto newMarketDto = new MarketDto();
		newMarketDto.setTicker(TEST_TICKER);

		logger.info("Testing MarketDto Object: " + newMarketDto);

		assertTrue("Invalid ticker returned", newMarketDto.getTicker() == TEST_TICKER);
	}

	@Test
	public void tickerConstructor_settersAndGetters() throws Exception {
		MarketDto newMarketDto = new MarketDto(TEST_TICKER);

		logger.info("Testing MarketDto Object: " + newMarketDto);

		assertTrue("Invalid ticker returned", newMarketDto.getTicker() == TEST_TICKER);
	}
}