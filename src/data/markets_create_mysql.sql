-- MySQL Markets

-- create schema and user 

create database if not exists markets;

CREATE USER IF NOT EXISTS markets identified by 'markets';

-- Update the password so it works
ALTER USER 'markets'@'%' IDENTIFIED WITH mysql_native_password BY 'markets';

GRANT ALL ON markets.* TO 'markets'@'%';

-- create tables

use markets;

-- drop table if exists Markets;

create table if not exists Markets (
    ticker      char(12)    not null,
    primary key (ticker)
);

-- insert sample data

insert into Markets values ('FTSE');
insert into Markets values ('NYSE');
insert into Markets values ('NASDAQ');
