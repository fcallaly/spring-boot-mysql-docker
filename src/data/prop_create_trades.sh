#!/bin/bash

# Script to create a properties file
# You will need to redirect to your own SQL file name
# You must have environment variables set of $DBNAME, $DBPASS, $DBUSER
# e.g. 
# prop_create_markets.sh > my_markets.sql
# Once ran you then use the my_markets.sql file to build you MySQL database

cat <<_END_
-- MySQL Markets

-- create schema and user 

create database if not exists $DBNAME;

CREATE USER IF NOT EXISTS $DBUSER identified by '$DBPASS';

GRANT ALL ON $DBNAME.* TO '$DBUSER'@'%';

-- create tables

use $DBNAME;

-- drop table if exists Markets;


create table Markets (
    ticker      char(12)    not null,
    primary key (ticker)
);

-- insert sample data

insert into Markets values ('FTSE');
insert into Markets values ('NYSE');
insert into Markets values ('NASDAQ');
_END_
