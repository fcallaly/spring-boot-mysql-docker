package com.neueda.training.market.mysql;

import java.util.regex.Pattern;

import javax.annotation.Priority;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.neueda.training.market.rest.DefaultExceptionHandler;

/**
 * Exception handling methods for MySQL. Avoid returning underlying errors messages from MySQL 
 * as these may contain information useful for hackers attempting to break a system 
 * (table or column names, underlying SQL statements etc.).
 * 
 * For MySQL exceptions the contents of the message have to be examined to determine the actual
 * cause see the {@link #dataIntegrityViolationExceptionHandler} for an example.
 * 
 *  @See com.neueda.training.market.rest.DefaultExceptionHandler
 *  @author Neueda
 *
 */
@ControllerAdvice
@Priority(10)
public class MySqlExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(MySqlExceptionHandler.class);


    Pattern nullColumn = Pattern.compile("Column '([^']+)' cannot be null");
    Pattern duplicateKey = Pattern.compile("Duplicate entry '([^']+)' for key '([^']+)'");
    	
    @ExceptionHandler(value = {DataIntegrityViolationException.class})
    public ResponseEntity<Object> dataIntegrityViolationExceptionHandler(HttpServletRequest request, DataIntegrityViolationException ex) {
        logger.warn(ex.toString());
        String msg = ex.getMessage();
        if (msg.contains("FOREIGN KEY (`market`)")) {
        	msg = "invalid market ticker";
        }
        else {
        	msg = "unidentified constraint failure";
        }
        return new ResponseEntity<>(new DefaultExceptionHandler.ErrorResponse("Failed: "+msg), HttpStatus.NOT_ACCEPTABLE);
    } 
}
