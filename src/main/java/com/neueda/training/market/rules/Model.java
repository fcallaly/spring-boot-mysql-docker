package com.neueda.training.market.rules;

import java.util.List;

import org.springframework.stereotype.Component;

import com.neueda.training.market.FatalMarketException;
import com.neueda.training.market.MarketException;
import com.neueda.training.market.model.Market;

/**
 * Utility class for managing the market business rules.
 * 
 * @author Neueda
 *
 */
@Component
public class Model {

    /**
     * Convenience method to check that DAO.findById() methods returns a single row.
     * 
     * @param <T>  Type of DTO to validate
     * @param <C>  Type of unique id (typically int or String)
     * @param list DTO rows returned from the query
     * @param id   query ID - used in the error message
     * @param type - DTO type name used in the error message
     * @return single DTO
     * @throws MarketException      if failed to find entry
     * @throws FatalMarketException more than one row matched (DB is corrupted)
     */
    public static final <T, C> T validateFindUnique(List<T> list, C id, String type) {
        // test most likely scenario first
        if (list.size() == 1) {
            return list.get(0);
        } else if (list.size() == 0) {
            throw new MarketException("Cannot find %s with id %s", type, id);
        } else {
            // defensive programming
            throw new FatalMarketException("Found duplicated %s id %s", type, id);
        }
    }

    public static final void validateMarket(Market market) {
        if (market.getTicker() == null || market.getTicker().equals("")) {
            throw new FatalMarketException("Invalid ticker %s", market.getTicker());
        }
    }
}
