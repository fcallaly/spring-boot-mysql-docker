package com.neueda.training.market;

@SuppressWarnings("serial")
public class MarketException extends RuntimeException
{
    public MarketException(String msg, Object... objs) {
        super(String.format(msg, objs));
    }
}

