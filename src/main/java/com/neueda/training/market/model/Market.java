package com.neueda.training.market.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Domain object for Country, only contains a name.
 * 
 * @author Neueda
 */
public interface Market {

    @NotNull(message = "ticker must be supplied")
    @Size(min = 1, max = 100, message = "ticker cannot be more than {max} characters")
	String getTicker();
	void setTicker(String ticker);
}
