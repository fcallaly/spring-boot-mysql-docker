package com.neueda.training.market.model;

/**
 * Factory functions required to create DTOs
 * 
 * @author Neueda
 *
 */
public interface DtoFactory {
	Market createMarket();
	Market createMarket(String market);
}
