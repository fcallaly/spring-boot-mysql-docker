package com.neueda.training.market.model;

import java.util.List;

import com.neueda.training.market.database.MarketDto;


public interface MarketDao {
    int rowCount();

    List<MarketDto> findAll();

    Market findByTicker(String ticker);

    Market create(Market market);
}
