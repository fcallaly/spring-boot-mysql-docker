package com.neueda.training.market;

@SuppressWarnings("serial")
public class FatalMarketException extends MarketException
{
    public FatalMarketException(String msg, Object... objs) {
        super(String.format(msg, objs));
    }
}