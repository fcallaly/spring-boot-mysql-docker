package com.neueda.training.market.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.neueda.training.market.database.MarketDto;
import com.neueda.training.market.model.Market;
import com.neueda.training.market.model.MarketDao;

@RestController
@RequestMapping("/markets")
public class MarketController {

    @Autowired
    private MarketDao marketDao;

    @RequestMapping(value = "/status/", method = RequestMethod.GET)
    public String status() {
        return "DB Status: OK\nNumber of Markets: " + marketDao.rowCount();
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<MarketDto> list() {
        return marketDao.findAll();
    }

    @RequestMapping(value = "/{ticker}", method = RequestMethod.GET)
    public Market find(@PathVariable String ticker) {
        return marketDao.findByTicker(ticker);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST,
                    consumes = { MediaType.APPLICATION_JSON_VALUE })
    public HttpEntity<Market> create(@RequestBody MarketDto newMarket) {
        return new ResponseEntity<Market>(marketDao.create(newMarket),
                                          HttpStatus.CREATED);
    }
}
